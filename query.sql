CREATE DATABASE QuanLySinhVien;
USE QuanLySinhVien;  
CREATE TABLE
    DMKHOA (
        MaKH VARCHAR(6) PRIMARY KEY,
        TenKhoa VARCHAR(30)
    );

CREATE Table
    SINHVIEN(
        MaSV VARCHAR(6) PRIMARY KEY,
        HoSV VARCHAR(30),
        TenSV VARCHAR(15),
        GioiTinh char(1),
        NgaySinh DATETIME,
        NoiSinh VARCHAR(50),
        DiaChi VARCHAR(50),
        MaKH VARCHAR(6),
        HocBong int,
        FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
    );

INSERT INTO `DMKHOA` VALUES ("MIM","Cong nghe thong tin");
INSERT INTO `DMKHOA` VALUES ("GEO","Dia ly");

INSERT INTO `SINHVIEN` VALUES ("111111","Nguyen Van", "A", "1", "2003/1/1", "HN", "HN","MIM",100000);
INSERT INTO `SINHVIEN` VALUES ("111113","Nguyen Van", "b", "1", "2003/1/1", "HN", "HN","GEO",100000);
SELECT * FROM `SINHVIEN` WHERE `MaKH` in (SELECT `MaKH` FROM `DMKHOA` WHERE `TenKhoa` like "Cong nghe thong tin");